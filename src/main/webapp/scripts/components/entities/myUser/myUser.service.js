'use strict';

angular.module('userscrudjhApp')
    .factory('MyUser', function ($resource) {
        return $resource('api/myUsers/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.createdDate = new Date(data.createdDate);
                    return data;
                }
            }
        });
    });
