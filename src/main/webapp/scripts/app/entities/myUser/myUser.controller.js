'use strict';

angular.module('userscrudjhApp')
    .controller('MyUserController', function ($scope, MyUser, ngTableParams, Language) {
        var PAGE_SIZE = 20;
        $scope.isAdminOptions = [{name: 'yes', value: "true"}, {name: 'no', value: "false"}, {name: 'all', value: ''}];
        $scope.filter = {isAdmin: ''};
        Language.getCurrent().then(function (language) {
            $scope.language = language;
        });
        $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {name: 'asc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                // Ajax call to update the table contents
                MyUser.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.tableParams.reload();
        };

        $scope.showErrors = function (userForm, fieldName) {
            if (userForm.hasOwnProperty(fieldName))
                return userForm[fieldName].$dirty && userForm[fieldName].$invalid;
            else
                return false;
        };

        $scope.create = function () {
            MyUser.save($scope.myUser,
                function () {
                    $scope.tableParams.reload();
                    $('#saveMyUserModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.myUser = MyUser.get({id: id});
            $('#saveMyUserModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.myUser = MyUser.get({id: id});
            $('#deleteMyUserConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            MyUser.delete({id: id},
                function () {
                    $scope.tableParams.reload();
                    $('#deleteMyUserConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.myUser = {name: null, age: null, isAdmin: null, createdDate: null, id: null};
            $scope.userForm.age.$dirty=false;
            $scope.userForm.name.$dirty=false;
        };
    });
