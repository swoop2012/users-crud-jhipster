'use strict';

angular.module('userscrudjhApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('myUser', {
                parent: 'entity',
                url: '/myUser',
                data: {
                    roles: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/myUser/myUsers.html',
                        controller: 'MyUserController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('myUser');
                        return $translate.refresh();
                    }]
                }
            })
            .state('myUserDetail', {
                parent: 'entity',
                url: '/myUser/:id',
                data: {
                    roles: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/myUser/myUser-detail.html',
                        controller: 'MyUserDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('myUser');
                        return $translate.refresh();
                    }]
                }
            });
    });
