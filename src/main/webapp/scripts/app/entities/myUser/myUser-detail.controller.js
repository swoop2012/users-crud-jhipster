'use strict';

angular.module('userscrudjhApp')
    .controller('MyUserDetailController', function ($scope, $stateParams, MyUser) {
        $scope.myUser = {};
        $scope.load = function (id) {
            MyUser.get({id: id}, function(result) {
              $scope.myUser = result;
            });
        };
        $scope.load($stateParams.id);
    });
