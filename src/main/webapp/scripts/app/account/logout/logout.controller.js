'use strict';

angular.module('userscrudjhApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
