package com.userscrud.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.userscrud.domain.MyUser;
import com.userscrud.model.MyUserSearch;
import com.userscrud.repository.MyUserRepository;
import com.userscrud.specification.MyUserSpecs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MyUser.
 */
@RestController
@RequestMapping("/api")
public class MyUserResource {

    private final Logger log = LoggerFactory.getLogger(MyUserResource.class);

    @Inject
    private MyUserRepository myUserRepository;

    /**
     * POST  /myUsers -> Create a new myUser.
     */
    @RequestMapping(value = "/myUsers",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody MyUser myUser) {
        log.debug("REST request to save MyUser : {}", myUser);
        myUserRepository.save(myUser);
    }

    /**
     * GET  /myUsers -> get all the myUsers.
     */
    @RequestMapping(value = "/myUsers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<MyUser> getAll(@ModelAttribute("user") MyUserSearch user, Pageable pageable) {
        log.debug("REST request to get all MyUsers");
        return myUserRepository.findAll(MyUserSpecs.getSearchCriteria(user), pageable);
    }

    /**
     * GET  /myUsers/:id -> get the "id" myUser.
     */
    @RequestMapping(value = "/myUsers/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MyUser> get(@PathVariable Long id) {
        log.debug("REST request to get MyUser : {}", id);
        return Optional.ofNullable(myUserRepository.findOne(id))
            .map(myUser -> new ResponseEntity<>(
                myUser,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /myUsers/:id -> delete the "id" myUser.
     */
    @RequestMapping(value = "/myUsers/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete MyUser : {}", id);
        myUserRepository.delete(id);
    }
}
