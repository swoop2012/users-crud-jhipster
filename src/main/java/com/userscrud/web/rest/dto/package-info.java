/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.userscrud.web.rest.dto;
