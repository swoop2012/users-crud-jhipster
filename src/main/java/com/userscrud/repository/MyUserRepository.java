package com.userscrud.repository;

import com.userscrud.domain.MyUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the MyUser entity.
 */
public interface MyUserRepository extends JpaRepository<MyUser,Long>{
    Page<MyUser> findAll(Specification<MyUser> specification, Pageable pageable);

}
