package com.userscrud.model;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MyUserSearch {

    private String  id;
    private String name;
    private String age;
    private String isAdmin;
    @DateTimeFormat(iso = ISO.DATE)
    private Date createdFrom;
    @DateTimeFormat(iso = ISO.DATE)
    private Date createdTo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Date getCreatedFrom() {
        return createdFrom;
    }

    public void setCreatedFrom(Date createdFrom) {
        this.createdFrom = createdFrom;
    }

    public Date getCreatedTo() {
        return createdTo;
    }

    public void setCreatedTo(Date createdTo) {
        this.createdTo = createdTo;
    }

    @Override
    public String toString() {
        return "UserSearch{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", isAdmin='" + isAdmin + '\'' +
                '}';
    }

    public void clear() {
        isAdmin = "";
        createdFrom = null;
        createdTo = null;
        age = "";
        name = "";
        id = "";
    }


}
