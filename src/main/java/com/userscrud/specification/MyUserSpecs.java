package com.userscrud.specification;


import com.userscrud.domain.MyUser;
import com.userscrud.model.MyUserSearch;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.*;


public class MyUserSpecs {

    public static Specification<MyUser> getSearchCriteria(final MyUserSearch model) {
        return new Specification<MyUser>() {
            public Predicate toPredicate(Root<MyUser> root, CriteriaQuery<?> query,
                                         CriteriaBuilder builder) {
                List<Predicate> predicates = new ArrayList<Predicate>();
                if(model.getId() != null && !model.getId().trim().equals(""))
                    predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));
                if(model.getName() != null && !model.getName().trim().equals(""))
                    predicates.add(builder.like(root.<String>get("name"), '%' + model.getName() + '%'));
                if(model.getAge() != null && !model.getAge().trim().equals(""))
                    predicates.add(builder.equal(root.<Integer>get("age"), model.getAge()));
                if(model.getCreatedFrom() != null) {
                    predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("createdDate"), model.getCreatedFrom()));
                }
                if(model.getCreatedTo() != null)
                {
                    Date dateTo = new Date( model.getCreatedTo().getTime() + 86_399_999);
                    predicates.add(builder.lessThanOrEqualTo(root.<Date>get("createdDate"), dateTo));
                }
                if(model.getIsAdmin() != null && !model.getIsAdmin().trim().equals(""))
                    predicates.add(builder.equal(root.<Boolean>get("isAdmin"), Boolean.valueOf(model.getIsAdmin())));
                return builder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
    }
}

