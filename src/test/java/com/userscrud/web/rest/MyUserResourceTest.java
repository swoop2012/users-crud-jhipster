package com.userscrud.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import com.userscrud.Application;
import com.userscrud.domain.MyUser;
import com.userscrud.repository.MyUserRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MyUserResource REST controller.
 *
 * @see MyUserResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class MyUserResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";

    private static final Integer DEFAULT_AGE = 0;
    private static final Integer UPDATED_AGE = 1;

    private static final Boolean DEFAULT_IS_ADMIN = false;
    private static final Boolean UPDATED_IS_ADMIN = true;

    private static final DateTime DEFAULT_CREATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CREATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CREATED_DATE_STR = dateTimeFormatter.print(DEFAULT_CREATED_DATE);

    @Inject
    private MyUserRepository myUserRepository;

    private MockMvc restMyUserMockMvc;

    private MyUser myUser;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MyUserResource myUserResource = new MyUserResource();
        ReflectionTestUtils.setField(myUserResource, "myUserRepository", myUserRepository);
        this.restMyUserMockMvc = MockMvcBuilders.standaloneSetup(myUserResource).build();
    }

    @Before
    public void initTest() {
        myUser = new MyUser();
        myUser.setName(DEFAULT_NAME);
        myUser.setAge(DEFAULT_AGE);
        myUser.setIsAdmin(DEFAULT_IS_ADMIN);
        myUser.setCreatedDate(DEFAULT_CREATED_DATE);
    }

    @Test
    @Transactional
    public void createMyUser() throws Exception {
        // Validate the database is empty
        assertThat(myUserRepository.findAll()).hasSize(0);

        // Create the MyUser
        restMyUserMockMvc.perform(post("/api/myUsers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(myUser)))
                .andExpect(status().isOk());

        // Validate the MyUser in the database
        List<MyUser> myUsers = myUserRepository.findAll();
        assertThat(myUsers).hasSize(1);
        MyUser testMyUser = myUsers.iterator().next();
        assertThat(testMyUser.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMyUser.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testMyUser.getIsAdmin()).isEqualTo(DEFAULT_IS_ADMIN);
        assertThat(testMyUser.getCreatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllMyUsers() throws Exception {
        // Initialize the database
        myUserRepository.saveAndFlush(myUser);

        // Get all the myUsers
        restMyUserMockMvc.perform(get("/api/myUsers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].id").value(myUser.getId().intValue()))
                .andExpect(jsonPath("$.[0].name").value(DEFAULT_NAME.toString()))
                .andExpect(jsonPath("$.[0].age").value(DEFAULT_AGE))
                .andExpect(jsonPath("$.[0].isAdmin").value(DEFAULT_IS_ADMIN.booleanValue()))
                .andExpect(jsonPath("$.[0].createdDate").value(DEFAULT_CREATED_DATE_STR));
    }

    @Test
    @Transactional
    public void getMyUser() throws Exception {
        // Initialize the database
        myUserRepository.saveAndFlush(myUser);

        // Get the myUser
        restMyUserMockMvc.perform(get("/api/myUsers/{id}", myUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(myUser.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE))
            .andExpect(jsonPath("$.isAdmin").value(DEFAULT_IS_ADMIN.booleanValue()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingMyUser() throws Exception {
        // Get the myUser
        restMyUserMockMvc.perform(get("/api/myUsers/{id}", 1L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMyUser() throws Exception {
        // Initialize the database
        myUserRepository.saveAndFlush(myUser);

        // Update the myUser
        myUser.setName(UPDATED_NAME);
        myUser.setAge(UPDATED_AGE);
        myUser.setIsAdmin(UPDATED_IS_ADMIN);
        myUser.setCreatedDate(UPDATED_CREATED_DATE);
        restMyUserMockMvc.perform(post("/api/myUsers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(myUser)))
                .andExpect(status().isOk());

        // Validate the MyUser in the database
        List<MyUser> myUsers = myUserRepository.findAll();
        assertThat(myUsers).hasSize(1);
        MyUser testMyUser = myUsers.iterator().next();
        assertThat(testMyUser.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMyUser.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testMyUser.getIsAdmin()).isEqualTo(UPDATED_IS_ADMIN);
        assertThat(testMyUser.getCreatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void deleteMyUser() throws Exception {
        // Initialize the database
        myUserRepository.saveAndFlush(myUser);

        // Get the myUser
        restMyUserMockMvc.perform(delete("/api/myUsers/{id}", myUser.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<MyUser> myUsers = myUserRepository.findAll();
        assertThat(myUsers).hasSize(0);
    }
}
